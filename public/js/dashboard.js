// Line
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Derivados", "Atendidos", "Pendientes", "Archivados", "Reinterados", "Reingresados"],
        datasets: [{
            label: 'Numero de expedientes',
            data: [120, 190, 30, 50, 20, 30],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

//pie
var ctxP = document.getElementById("pieChart").getContext('2d');
var myPieChart = new Chart(ctxP, {
    type: 'pie',
    data: {
        labels: ["Derivados", "Atendidos", "Pendientes", "Archivados", "Reinterados", "Reingresados"],
        datasets: [
            {
                data: [120, 190, 30, 50, 20, 30],
                backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
                hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
            }
        ]
    },
    options: {
        responsive: true
    }
});


//line
var ctxL = document.getElementById("lineChart").getContext('2d');
var myLineChart = new Chart(ctxL, {
    type: 'line',
    data: {
        labels: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes"],
        datasets: [
            {
                label: "Semana pasada",
                fillColor: "#F7464A",
                strokeColor: "#F7464A",
                pointColor: "#F7464A",
                pointStrokeColor: "#F7464A",
                pointHighlightFill: "#F7464A",
                pointHighlightStroke: "#F7464A",
                data: [25,12,31,18,22]
            },
        ]
    },
    options: {
        responsive: true
    }
});


//radar
var ctxR = document.getElementById("radarChart").getContext('2d');
var myRadarChart = new Chart(ctxR, {
    type: 'radar',
    data: {
        labels: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes"],
        datasets: [
            {
                label: "Semana pasada",
                fillColor: "#F7464A",
                strokeColor: "#F7464A",
                pointColor: "#F7464A",
                pointStrokeColor: "#F7464A",
                pointHighlightFill: "#F7464A",
                pointHighlightStroke: "#F7464A",
                data: [25,12,31,18,22]
            },
        ]
    },
    options: {
        responsive: true
    }
});

//doughnut
var ctxD = document.getElementById("doughnutChart").getContext('2d');
var myLineChart = new Chart(ctxD, {
    type: 'doughnut',
    data: {
        labels: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes"],
        datasets: [
            {
                data: [30, 10, 36, 25, 14],
                backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
                hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
            }
        ]
    },
    options: {
        responsive: true
    }
});

