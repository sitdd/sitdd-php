<?php

use Illuminate\Database\Seeder;

class Usuario extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Usuario')->insert([
            'cNombres' => 'Anthony Wainer',
            'cApellidos' => 'Cachay Guivin',
            'cUsuario' => 'acachay',
            'password' => bcrypt('apci2018'),
            'cCorreo' => 'acachay@apci.gob.pe',
            'fkTipoDocIdentidad' => 1,
            'cDocIdentidad'=> '71535030',
        ]);
    }
}
