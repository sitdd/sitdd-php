@extends('layouts.app')

@section('content')
<div class="container h-100">
    <div class="row justify-content-center h-100">
        <div class="col-md-8 align-self-center">

            <style>

                .card{
                    color: white;
                    background-color: #164d6bbf!important;
                    border-color: #282b314f!important;
                    border-radius: 10px;
                }

                .btn{
                    border-radius: 20px;
                }

                .form-control {
                    color: white;
                }
                .invalid-feedback{
                    text-align: center;
                }

            </style>

            <div class="card">
                <div class="card-header text-center">{{ __('Restablecer contraseña') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Restablecer contraseña') }}">
                        @csrf

                        <div class="row justify-content-center">

                            <div class="col-md-6 align-self-center">

                                <div class="md-form">
                                    <i class="fa fa-envelope prefix"></i>
                                    <input type="email" id="inputValidationEx" class="form-control validate text-white {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                                    <label class="text-white" for="inputValidationEx" data-error="Incorrecto" data-success="Correcto">{{ __('Correo electrónico') }}</label>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif

                                </div>

                                <div class="row justify-content-center">
                                <button type="submit" class="btn grey darken-4">
                                    {{ __('Restablecer') }}
                                </button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
