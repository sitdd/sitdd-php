@extends('layouts.app')

@section('styles')
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container-fluid h-100">

        <div class="row h-100 justify-content-center align-items-center">

            <div class="col col-sm-8 col-md-6 col-lg-5 col-xl-4">

                <div class="login-container w-75 mx-auto">

                    <!--form login -->

                    <form class="form-login text-center p-4" method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf

                        <i class="far fa-user icon" id="iconlogin"></i>

                        <p class="h4 icon">{{ __('Iniciar Sesión') }}</p>


                        <!--User id-->
                        <div class="md-form text-white text-left">
                            <i class="fas fa-user-circle icon prefix"></i>
                            <input type="text" name="login" id="usuario" class="form-control validate text-white {{ $errors->has('username') || $errors->has('email') ? ' is-invalid' : '' }}"
                                   value="{{ old('username') ?: old('email') }}"
                            >
                            <label class="text-login text-white" for="usuario" data-error="Incorrecto" data-success="Correcto">{{ __('Usuario o Correo') }}</label>
                        </div>
                        <!--/.User id-->
                        @if ($errors->has('username') || $errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                         <strong>{{ $errors->first('username') ?: $errors->first('email') }}</strong>
                                    </span>
                        @endif

                        <!--Password-->
                        <div class="md-form text-white text-left">
                            <i class="fa fa-lock icon prefix"></i>
                            <input type="password" id="contraseña" name="password" class="form-control validate text-white mb-0 {{ $errors->has('password') ? ' is-invalid' : '' }}">
                            <label class="text-login text-white" for="contraseña" data-error="Incorrecto" data-success="Correcto">{{ __('Contraseña') }}</label>
                        </div>
                        <!--/.Password-->
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif

                        <div class="d-flex justify-content-around">
                            <div>
                                <!-- Remember me -->
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="recordar" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label text-white" for="recordar">{{ __('Recordarme') }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center">
                        <!-- Sign in button -->
                        <button class="btn  grey darken-4" type="submit">{{ __('Ingresar') }}</button>
                        <a class="btn btn-link text-center" href="{{ route('password.request') }}">
                            {{ __('¿Olvidaste tu Contraseña?') }}
                        </a>
                        </div>

                    </form>
                    <!--/.form login -->
                </div>

            </div>

        </div>


    </div>
@endsection
