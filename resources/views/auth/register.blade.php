@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registro') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombres') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('cNombres') ? ' is-invalid' : '' }}"
                                       name="cNombres" value="{{ old('cNombres') }}" required autofocus>

                                @if ($errors->has('cNombres'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cNombres') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Apellidos') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('cApellidos') ? ' is-invalid' : '' }}"
                                       name="cApellidos" value="{{ old('cApellidos') }}" required autofocus>

                                @if ($errors->has('cApellidos'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cApellidos') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right">
                                {{ __('Usuario') }}
                            </label>

                            <div class="col-md-6">
                                <input id="username" type="text"
                                       class="form-control{{ $errors->has('cUsuario') ? ' is-invalid' : '' }}"
                                       name="cUsuario" value="{{ old('cUsuario') }}" required>

                                @if ($errors->has('cUsuario'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('cUsuario') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('cCorreo') ? ' is-invalid' : '' }}" name="cCorreo" value="{{ old('cCorreo') }}" required>

                                @if ($errors->has('cCorreo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cCorreo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tip" class="col-md-4 col-form-label text-md-right">{{ __('Tipo de Documento') }}</label>

                            <div class="col-md-6">
                                <input id="tip" type="text" class="form-control{{ $errors->has('fkTipoDocIdentidad') ? ' is-invalid' : '' }}" name="fkTipoDocIdentidad" value="{{ old('fkTipoDocIdentidad') }}" required>

                                @if ($errors->has('fkTipoDocIdentidad'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fkTipoDocIdentidad') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="doc" class="col-md-4 col-form-label text-md-right">{{ __('N° de Documento') }}</label>

                            <div class="col-md-6">
                                <input id="doc" type="text" class="form-control{{ $errors->has('cDocIdentidad') ? ' is-invalid' : '' }}" name="cDocIdentidad" value="{{ old('cDocIdentidad') }}" required>

                                @if ($errors->has('cDocIdentidad'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cDocIdentidad') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
