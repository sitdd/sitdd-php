@extends('layouts.admin')

@section('dashboard')

    <!--Loading-->
    <div class="loading-row row justify-content-center fixed-top d-none">

        <div class="col text-center align-self-center">

            <div class="loader">
                <div class="loader-inner box1"></div>
                <div class="loader-inner box2"></div>
                <div class="loader-inner box3"></div>
            </div>

        </div>
    </div>
    <!--/.loading-->

    <!--Main layout-->
    <main class="mx-lg-5">
        <div class="container-fluid">




        </div>
    </main>
    <!--Main layout-->


@endsection
