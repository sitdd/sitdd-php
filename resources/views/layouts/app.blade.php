<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<body>

    @yield('content')


    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="{!! asset('/js/jquery-3.3.1.min.js') !!}"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{ asset('/js/popper.min.js') }}"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{ asset('/js/mdb.min.js') }}"></script>

    @section('scripts')
        @include('layouts.partials.scripts')
    @show
</body>
</html>
