@extends('layouts.app')

@section('styles')
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/loading.css') }}" rel="stylesheet">
@endsection

@section('content')
    <header>

        <div class="firstmenu">

            <!--Navbar-->
            <nav class="navbar navbar-expand-lg navbar-dark blue darken-3 fixed-top clearfix" id="navegacion">

                <a class="navbar-brand" href="{{ url('/') }}"><b>SITDD</b></a>

                <!-- Siempre presente -->

                <div class="navbar-nav ml-auto">
                    <div class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-transform: capitalize">
                            <img class="img-profile-menu" src="img/foto.jpg" alt="{{ Auth::user()->cUsuario }} foto"> &nbsp;{{ Auth::user()->cUsuario }}</a>
                        <div class="dropdown-menu dropdown-menu-right " aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="{{route('profile')}}"><i class="fas fa-user"></i>&nbsp;Perfil</a>
                            <a class="dropdown-item" href="{{route('changePassword')}}"><i class="fas fa-key"></i>&nbsp;Contraseña</a>
                            <a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit()"><i class="fas fa-sign-out-alt"></i>&nbsp;Salida</a>
                        </div>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                    </div>
                    <!-- fin Siempre presente -->
                </div>

            </nav>
            <!--/.Navbar-->

            <!--Menu botones flotantes-->
            <div class="clearfix float-left fixed-top menu-flotante">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="dropdown dropright">
                    <button class="btn btn-floating boton-flotante blue darken-3" type="button" id="dropmenulink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-registered"></i></button>
                    <div class="dropdown-menu" aria-labelledby="dropmenulink">
                        <p class="dropdown-header droptitulo">REGISTRO<p>
                            <a class="dropdown-item" href="#">Externo</a>
                            <a class="dropdown-item" href="#">Interno</a>
                    </div>
                </div>
                <div class="dropdown dropright">
                    <button class="btn btn-floating boton-flotante blue darken-3" type="button" id="dropmenulink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-envelope"></i></button>
                    <div class="dropdown-menu" aria-labelledby="dropmenulink">
                        <p class="dropdown-header droptitulo">BANDEJA<p>
                            <a class="dropdown-item" href="#">Pendiente</a>
                            <a class="dropdown-item" href="#">Derivados</a>
                    </div>
                </div>
                <div class="dropdown dropright">
                    <button class="btn btn-floating boton-flotante blue darken-3" type="button" id="dropmenulink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-search"></i></button>
                    <div class="dropdown-menu" aria-labelledby="dropmenulink">
                        <p class="dropdown-header droptitulo">CONSULTA<p>
                            <a class="dropdown-item" href="#">Consulta Pendientes</a>
                            <a class="dropdown-item" href="#">Reporte</a>
                    </div>
                </div>
                <div class="dropdown dropright">
                    <button class="btn btn-floating boton-flotante blue darken-3" type="button" id="dropmenulink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-wrench"></i></button>
                    <div class="dropdown-menu" aria-labelledby="dropmenulink">
                        <p class="dropdown-header droptitulo">MANTENIMIENTO<p>
                            <a class="dropdown-item" href="#">M. Perfiles</a>
                            <a class="dropdown-item" href="#">M. Modulos</a>
                            <a class="dropdown-item" href="#">M. Trabajadores</a>
                    </div>
                </div>

            </div>
            <!--/. Menu botones flotantes-->




        </div>


        <div class="secondmenu">
            <!--Navbar-->
            <nav class="navbar navbar-expand-lg navbar-dark blue darken-3 fixed-top" id="navegacion">

                <!-- Navbar brand -->
                <a class="navbar-brand" href="#"><b>SITDD</b></a>

                <!-- Collapse button -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- Collapsible content -->
                <div class="collapse navbar-collapse" id="navbarNav">

                    <!-- Links -->
                    <div class="navbar-nav ml-auto">
                        <div class="nav-item dropdown">
                            <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-registered"></i>&nbsp;Registro<span class="dropdown-toggle d-inline"></span></a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropmenulink">
                                <p class="dropdown-header droptitulo">REGISTRO<p>
                                    <a class="dropdown-item" href="#">Externo</a>
                                    <a class="dropdown-item" href="#">Interno</a>
                            </div>
                        </div>
                        <div class="nav-item dropdown">
                            <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-envelope"></i>&nbsp;Bandeja<span class="dropdown-toggle d-inline"></span></a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropmenulink">
                                <p class="dropdown-header droptitulo">BANDEJA<p>
                                    <a class="dropdown-item" href="#">Pendiente</a>
                                    <a class="dropdown-item" href="#">Derivados</a>
                            </div>
                        </div>
                        <div class="nav-item dropdown">
                            <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-search"></i>&nbsp;Consulta<span class="dropdown-toggle d-inline"></span></a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropmenulink">
                                <p class="dropdown-header droptitulo">CONSULTA<p>
                                    <a class="dropdown-item" href="#">Consulta Pendientes</a>
                                    <a class="dropdown-item" href="#">Reporte</a>
                            </div>
                        </div>
                        <div class="nav-item dropdown">
                            <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-wrench"></i>&nbsp;Mantenimiento<span class="dropdown-toggle d-inline"></span></a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropmenulink">
                                <p class="dropdown-header droptitulo">MANTENIMIENTO<p>
                                    <a class="dropdown-item" href="#">M. Perfiles</a>
                                    <a class="dropdown-item" href="#">M. Modulos</a>
                                    <a class="dropdown-item" href="#">M. Trabajadores</a>
                            </div>
                        </div>
                        <!-- Siempre presente -->
                        <div class="nav-item dropdown">
                            <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-transform: capitalize"><img class="img-profile-menu" src="img/foto.jpg" alt="{{ Auth::user()->cUsuario }} foto"> &nbsp;{{ Auth::user()->cUsuario }}<span class="dropdown-toggle d-inline"></span></a>
                            <div class="dropdown-menu dropdown-menu-right " aria-labelledby="navbarDropdownMenuLink">
                                <p class="dropdown-header droptitulo" style="text-transform: uppercase ">{{ Auth::user()->cUsuario }}<p>
                                    <a class="dropdown-item" href="#"><i class="fas fa-user"></i>&nbsp;Perfil</a>
                                    <a class="dropdown-item" href="#"><i class="fas fa-key"></i>&nbsp;Contraseña</a>
                                    <a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit()"><i class="fas fa-sign-out-alt"></i>&nbsp;Salida</a>
                            </div>
                        </div>
                        <!-- fin Siempre presente -->

                    </div>
                    <!-- Links -->


                </div>
                <!-- Collapsible content -->

            </nav>
            <!--/.Navbar-->
        </div>



    </header>
    <!--Main Navigation-->

    @yield('dashboard')

    <!--Footer-->
    <footer class="page-footer fixed-bottom font-small blue darken-4 text-center">
        <span id="footerGreeting"></span>, Bienvenido al Sistema de Trámite Documentario Digital de la APCI
    </footer>
    <!--/.Footer-->

@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('/js/dashboard.js') }}"></script>
@endsection