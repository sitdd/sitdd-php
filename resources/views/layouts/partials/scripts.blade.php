<!-- Initializations -->
<script type="text/javascript">
    // Animations initialization
    new WOW().init();
</script>

<!--Saludo del footer-->
<script>
    var d = new Date();
    var n = d.getHours();

    if(n<12){
        document.getElementById("footerGreeting").innerHTML = "Buenos Dias";
    }else if(n<19){
        document.getElementById("footerGreeting").innerHTML = "Buenos Tardes";
    }else{
        document.getElementById("footerGreeting").innerHTML = "Buenos Noches";
    }
</script>